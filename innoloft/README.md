# Innoloft

## Team

### Web Design & Development

[Luca Cattide](@lucacattide)

<info@lucacattide.dev> (https://lucacattide.dev)

## FAQ

I put various comments around the sources to clarify some of my choices.

### ETD

Orientatively 8 hours, including: setup, configurations, tweaks.

## Tools/Improvements

In addition to the main specifications:

- Code linting/formatting (ESLint/Prettier);
- CSS pre-processing (SASS);
- Webfonts (Google Fonts, Font Awesome);
- Data fetching (Isomorphic Unfetch);
- HTTP/2:
  - Prefetching;
  - Preloading;
- SEO:
  - File naming conventions;
  - Meta tags;
  - Structured data;
  - robots.txt;
  - Sitemap;
